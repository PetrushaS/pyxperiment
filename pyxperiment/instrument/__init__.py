"""
    pyxperiment/instrument: Describes instruments and their capabilities

    This file is part of the PyXperiment project.

    Copyright (c) 2023 PyXperiment Developers

    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the "Software"), to deal
    in the Software without restriction, including without limitation the rights
    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
    copies of the Software, and to permit persons to whom the Software is
    furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included in
    all copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
    THE SOFTWARE.
"""

__all__ = [
    'instrument',
    'instrument_module',
    'validation',
    'instrument_control',
    'tcp_instrument',
    'visa_instrument',
    'instrument_factory',
    ]

# Make core features accessible in controller namespace
from .instrument import Instrument
from .instrument_module import InstrumentModule
from .validation import Validator, EmptyValidator, StaticRangeValidator, DynamicRangeValidator
from .instrument_control import (
    InstrumentControl, ValueControl, RampControl, ListControl, SweepControl, BooleanControl,
    ActionControl, StateControl, TimeoutControl, MultiControl
)
from .tcp_instrument import TcpSocketInstrument
from .visa_instrument import VisaInstrument
from .zurich_instrument import ZurichInstrument
from .instrument_factory import InstrumentFactory
