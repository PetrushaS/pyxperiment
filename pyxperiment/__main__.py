"""
    pyxperiment/main.py: The entry module for PyXperiment user-friendly application

    This file is part of the PyXperiment project.

    Copyright (c) 2023 PyXperiment Developers

    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the "Software"), to deal
    in the Software without restriction, including without limitation the rights
    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
    copies of the Software, and to permit persons to whom the Software is
    furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included in
    all copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
    THE SOFTWARE.
"""

from pyxperiment.core.application import PyXperimentApp

# Application startup
if __name__ == '__main__':
    # Initialization
    try:
        APP = PyXperimentApp()
    except OSError as exc:
        from pyxperiment.frames.app_settings_frame import AppSettingsFrame
        PyXperimentApp().frame = AppSettingsFrame(None)
        PyXperimentApp().start()
        # Kill the application with the exception, for it to get logged
        raise exc
    # Create the main frame and start the application
    from pyxperiment.frames.experiment_setup_frame import ExperimentSetupFrame
    APP.frame = ExperimentSetupFrame(
        APP.resource_manager,
        APP.settings.get_readers(),
        APP.settings.get_writers()
        )
    APP.start()
