"""
    pyxperiment/devices/keithley/keithley2400.py: Support for Keithley 2400 SMU

    This file is part of the PyXperiment project.

    Copyright (c) 2019 PyXperiment Developers

    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the "Software"), to deal
    in the Software without restriction, including without limitation the rights
    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
    copies of the Software, and to permit persons to whom the Software is
    furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included in
    all copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
    THE SOFTWARE.
"""

from pyxperiment.instrument import (
    VisaInstrument, ValueControl
)

class Keithley2400SMU(VisaInstrument):
    """
    Keithley 2400 SMU support
    """

    def __init__(self, rm, resource):
        super().__init__(rm, resource)
        self.inst.write_termination = '\r\n'
        self.inst.read_termination = '\r\n'
        self.write('*RST')
        self.write(':SYST:AZER ON')
        self.write(':SOUR:FUNC CURR')

    @staticmethod
    def driver_name():
        return 'Keithley 240x SMU'

    def query(self, data):
        self.inst.write(data, '\n')
        value = self.read()
        return value

    def device_name(self):
        value = self.query_id().translate({ord(c): None for c in ['\r', '\n']}).split(',')
        return value[0] + ' ' + value[1] + ' SMU'

    def reset(self):
        self.write('*RST')

    value = ValueControl(
        'Source current', 'A',
        get_func=lambda instr: instr.query(':SOUR:CURR?'),
        set_func=lambda instr, value: instr.write(':SOUR:CURR ' + str(value)),
    )
